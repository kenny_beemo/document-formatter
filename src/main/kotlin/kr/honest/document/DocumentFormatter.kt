package kr.honest.document

import kr.honest.document.annotation.Align
import kr.honest.document.annotation.Document
import kr.honest.document.annotation.Format
import java.math.BigInteger
import kotlin.reflect.KAnnotatedElement
import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties
import kotlin.reflect.jvm.jvmErasure

object DocumentFormatter {
  fun <T : Any> export(document: T): String {
    val documentAnnotation = document::class.findAnnotation<Document>()
            ?: throw Exception("source is not a Document file")

    val propertiesPair = document::class.memberProperties
            .filter { it.findAnnotation<Format>() != null }
            .map { it to it.findAnnotation<Format>()!! }
            .sortedBy { it.second.index }

    // validate
    val totalLength = documentAnnotation.length
    val sumOfPropertiesLength = propertiesPair.map { it.second.length }.sum()
    if (totalLength != sumOfPropertiesLength) {
      throw Exception("Document.length ($totalLength) is not equal to sum of properties.length ($sumOfPropertiesLength)")
    }

    if (propertiesPair.size != propertiesPair.distinctBy { it.second.index }.size) {
      throw Exception("invalid index")
    }

    return propertiesPair.map { (property, annotation) ->
      serialize(document, property, annotation)
    }.joinToString(separator = "", truncated = "") { it }
  }

  fun <T : Any> load(str: String, cls: KClass<T>): T {
    val documentAnnotation = cls.findAnnotation<Document>()
            ?: throw Exception("cls (${cls.simpleName}) is not documented class")

    // validation
    if (str.length != documentAnnotation.length) {
      throw Exception("Document.length (${documentAnnotation.length}) is not equal to str.length (${str.length})")
    }

    val propertiesPair = cls.memberProperties
            .filter { it.findAnnotation<Format>() != null }
            .map { it to it.findAnnotation<Format>()!! }
            .sortedBy { it.second.index }

    val constructor = cls.constructors.single()
    val kParameterList = constructor.parameters
    val parameterMap = propertiesPair.map { (property, annotation) ->
      val startIndex = propertiesPair.map { it.second }
              .filter { it.index < annotation.index }
              .sumBy { it.length }

      val value = deserialize(str, startIndex, property, annotation)

      val kParameter = kParameterList.single { it.name == property.name }
      kParameter to value
    }.toMap()

    return constructor.callBy(parameterMap)
  }

  private fun <T: Any> serialize(document: T, property: KProperty1<out T, Any?>, annotation: Format): String {
    @Suppress("UNCHECKED_CAST")
    val value = when (property.returnType.classifier) {
      Int::class -> {
        (property as KProperty1<T, Int>).get(document).toString()
      }
      Long::class -> {
        (property as KProperty1<T, Long>).get(document).toString()
      }
      BigInteger::class -> {
        (property as KProperty1<T, BigInteger>).get(document).toString()
      }
      String::class -> {
        (property as KProperty1<T, String>).get(document)
      }
      else -> {
        if ((property.returnType.classifier as? KAnnotatedElement)?.findAnnotation<Document>() != null) {
          export((property as KProperty1<T, Any>).get(document))
        } else {
          throw KotlinReflectionNotSupportedError("kotlin type ${property.returnType} is not supported")
        }
      }
    }

    // validation
    if (value.length > annotation.length) {
      throw Exception("property ${property.name} has longer length data (${value.length}) than defined in annotation (${annotation.length})")
    }

    return when (annotation.align) {
      Align.LEFT -> {
        value.padEnd(
                length = annotation.length,
                padChar = annotation.paddingChar
        )
      }
      Align.RIGHT -> {
        value.padStart(
                length = annotation.length,
                padChar = annotation.paddingChar
        )
      }
      Align.EXACT -> {
        if (value.length != annotation.length) {
          throw Exception("exact value.length(${value.length}) must be same to annotation.length(${annotation.length})")
        }
        value
      }
    }
  }

  private fun <T: Any> deserialize(str: String, startIndex: Int, property: KProperty1<T, *>, annotation: Format): Any {
    val valueAsString = str.substring(startIndex, startIndex + annotation.length)

    val trimmedValue = when (annotation.align) {
      Align.LEFT -> {
        valueAsString.trimEnd(annotation.paddingChar)
      }
      Align.RIGHT -> {
        valueAsString.trimStart(annotation.paddingChar)
      }
      Align.EXACT -> {
        valueAsString
      }
    }

    return when (property.returnType.classifier) {
      Int::class -> {
        if(trimmedValue.isBlank()) 0 else trimmedValue.toInt()
      }
      Long::class -> {
        if (trimmedValue.isBlank()) 0 else trimmedValue.toLong()
      }
      String::class -> {
        trimmedValue
      }
      BigInteger::class -> {
        if (trimmedValue.isBlank()) BigInteger.ZERO else BigInteger(trimmedValue)
      }
      else -> {
        if ((property.returnType.classifier as? KAnnotatedElement)?.findAnnotation<Document>() != null) {
          load(trimmedValue, property.returnType.jvmErasure)
        } else {
          throw KotlinReflectionNotSupportedError("kotlin type ${property.returnType} is not supported")
        }
      }
    }
  }
}