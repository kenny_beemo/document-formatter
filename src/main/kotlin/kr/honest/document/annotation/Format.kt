package kr.honest.document.annotation

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.PROPERTY)
annotation class Format (
        val index: Int,
        val length: Int,
        val align: Align = Align.LEFT,
        val paddingChar: Char = ' '
)