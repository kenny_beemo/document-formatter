package kr.honest.document.annotation

enum class Align {
  LEFT,
  RIGHT,
  EXACT
}