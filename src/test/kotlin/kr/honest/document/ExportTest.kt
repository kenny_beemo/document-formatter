package kr.honest.document

import kr.honest.document.annotation.Align
import kr.honest.document.annotation.Document
import kr.honest.document.annotation.Format
import org.junit.Test
import java.math.BigInteger

class ExportTest {

  @Test
  fun test() {
    val inputData = TestData(
            a =1,
            b = 2,
            c = "a",
            d = BigInteger("123456789"),
            g = InnerData(
                    e = 100,
                    f ="a"
            )
    )
    println(inputData)
    val str = DocumentFormatter.export(inputData)
    println(str)

    val outputData = DocumentFormatter.load(str, TestData::class)
    println(outputData)

    assert(inputData == outputData) { "ㅠㅠ" }
  }

  @Document(30)
  data class TestData(
          @Format(index = 1, length = 2, align = Align.LEFT, paddingChar = '0') val a: Int,
          @Format(index = 2, length = 2, align = Align.RIGHT, paddingChar = '0') val b: Long,
          @Format(index = 3, length = 6, align = Align.RIGHT, paddingChar = ' ') val c: String,
          @Format(index = 4, length = 10, align = Align.RIGHT, paddingChar = '0') val d: BigInteger,
          @Format(index = 5, length = 10, align = Align.EXACT, paddingChar = ' ') val g: InnerData
  )

  @Document(10)
  data class InnerData(
          @Format(index = 1, length = 5, align = Align.RIGHT, paddingChar = '0') val e: Int,
          @Format(index = 2, length = 5, align = Align.LEFT, paddingChar = ' ') val f: String
  )
}